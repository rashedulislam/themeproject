<!-- <div class="row"> -->
<div class="col-md-offset-1 col-md-3 section-right">
    <h4>Most commented post</h4>
    <ul>

    <?php
           $args = array(
                'post_type'=>'post',
                'posts_per_page'=> 3,
                'orderby' => 'comment_count',
                'ignore_sticky_posts' => true
                );

            $the_query = new WP_Query( $args ); ?>
            
            <?php if ( $the_query->have_posts() ) : ?>

            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <a href="<?php the_permalink(); ?>">
                    <li><?php the_title(); ?></li>
                </a>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
            
            <?php else : ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?> ;
    </ul>

    <h4>Older post</h4>
    <ul>
         <?php 
            $args = array(
                'post_type'=>'post',
                'posts_per_page'=> 3,
                'order'     => 'ASC',
                'ignore_sticky_posts' => true
                );

            $the_query = new WP_Query( $args ); ?>
            
            <?php if ( $the_query->have_posts() ) : ?>

            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <a href="<?php the_permalink(); ?>">
                    <li><?php the_title(); ?></li>
                </a>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
            
            <?php else : ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?> ;
    </ul>

    <?php 
      $categories = get_categories([
        'orderby'  => 'count',
        'order'    => 'ASC',
        'number'   => 3
    ]);
    $category_name  = $categories[0]->cat_name;
    $category_name2 = $categories[1]->cat_name;
    $category_name3 = $categories[2]->cat_name;
    $post_count     = $categories[0]->count;
    $post_count2    = $categories[1]->count;
    $post_count3    = $categories[2]->count;

    $i = 0;
    
    if($post_count>=3){ ?>
        <h4>Most used category (<?php echo $category_name;?>) post</h4>
   <?php } elseif($post_count + $post_count2 >=3){ ?>
        <h4>Most used category (<?php echo $category_name . ', ' . $category_name2 ;?>) post</h4>
   <?php } else{ ?>
        <h4>Most used category (<?php echo $category_name . ', ' . $category_name2 . ', ' . $category_name3 ;?>) post</h4>
   <?php } ?>

   <ul>


    <?php $args = array(
            'category_name' => $category_name,
            'posts_per_page'=> 3,
        );
        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) :
            while ( $the_query->have_posts() ) : $the_query->the_post();
                if($i>=3) break; ?>
                <a href="<?php the_permalink(); ?>"><li><?php the_title(); ?></li></a>
        <?php $i++;
            endwhile;
            wp_reset_postdata();
        endif;



        $args = array(
            'category_name' => $category_name2,
            'posts_per_page'=> 3,
        );
        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) :
            while ( $the_query->have_posts() ) : $the_query->the_post();
                if($i>=3) break; ?>
                    <a href="<?php the_permalink(); ?>"><li><?php the_title(); ?></li></a>
            <?php   $i++;
            endwhile;
            wp_reset_postdata();
        endif;



        $args = array(
            'category_name' => $category_name3,
            'posts_per_page'=> 3,
        );
        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) :
            while ( $the_query->have_posts() ) : $the_query->the_post();
                if($i>=3) break; ?>
                <a href="<?php the_permalink(); ?>"><li><?php the_title(); ?></li></a> 
        <?php  $i++;
            endwhile;
            wp_reset_postdata();
        endif; ?>

    </ul>


    <h4>Most used category</h4>
    <ul>

    <!-- $args = array(
            'orderby'  => 'count',
            'order'    => 'DESC',
            'number'   => 3
        );
        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) :
            while ( $the_query->have_posts() ) : $the_query->the_post();
                if($i>=3) break; ?>
                    <a href="<?php //the_permalink(); ?>"><li><?php //the_title(); ?></li></a>
            <?php //endwhile;
            //wp_reset_postdata();
            //endif; ?> -->

        <?php 
            $categories = get_categories([
            'orderby'  => 'count',
            'order'    => 'DESC',
            'number'   => 3
        ]);

        foreach ($categories as $category) { ?>
            
            <a href="<?php echo get_category_link($category->cat_ID); ?>"><li><?php echo $category->cat_name .'(' .$category->count . ')' ;?></li></a>

        <?php } ?>

    </ul>

    <h4>Most used tag</h4>
    <ul>
        <?php 
            $tags = get_tags([
            'orderby'  => 'count',
            'order'    => 'DESC',
            'number'   => 3
             ]);

        foreach ($tags as $tag) { ?>
            
            <a href="<?php echo get_tag_link($tag->term_id); ?>"><li><?php echo $tag->name .'(' .$tag->count . ')' ;?></li></a>

        <?php } ?>
    </ul>
    <h4>Less used category </h4>
    <ul>
        <?php 
            $categories = get_categories([
            'orderby'  => 'count',
            'order'    => 'ASC',
            'number'   => 3
            ]);

        foreach ($categories as $category) { ?>
            
            <a href="<?php echo get_category_link($category->cat_ID); ?>"><li><?php echo $category->cat_name .'(' .$category->count . ')' ;?></li></a>

        <?php } ?>
    </ul>
    <h4>Less used tags </h4>
    <ul>
    <?php 
            $tags = get_tags([
            'orderby'  => 'count',
            'order'    => 'ASC',
            'number'   => 3
             ]);

        foreach ($tags as $tag) { ?>
            
            <a href="<?php echo get_tag_link($tag->term_id); ?>"><li><?php echo $tag->name .'(' .$tag->count . ')' ;?></li></a>

        <?php } ?>
    </ul>
</div>
