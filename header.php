<!DOCTYPE html>
<html <?php language_attributes();?>>  
<head>
	<meta charset="<?php bloginfo('charset');?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="pingback" href="<?php bloginfo('pingback_url');?>" />

	<?php wp_head();?>
 </head>
 <body <?php body_class(); ?>>
 <div class="container">
	<header>
		<div class="top-header">
			<div class="container">
				<div class="logo col-md-12 text-center">
				<a href="<?php echo site_url();?>">
					<p>Logo</p>
				</a>
				</div>
			</div>
		</div>
		<div class="container">
			<nav class="navbar navbar-default">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				</div>

			
					<?php
								wp_nav_menu( array(
								'theme_location'  => 'header_menu',
								'depth'           => 1,
								'container'       => 'div',
								'container_class' => 'collapse navbar-collapse',
								'container_id'    => 'bs-example-navbar-collapse-1',
								'menu_class'      => 'nav navbar-nav',
								'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
								'walker'          => new WP_Bootstrap_Navwalker(),
								
							) ); ?>
			
				</nav>

		</div>
	</header>
