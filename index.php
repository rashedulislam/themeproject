<?php get_header(); ?>

   <!-- main section -->
   <section class="container">
         <div class="col-md-8">

		 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 
			 
      //  $archive_year  = get_the_time('Y'); 
      //  $archive_month = get_the_time('m'); 
      //  $archive_day   = get_the_time('d'); 
		 ?>
            <div class="title">
			   <h2><?php the_title();?></h2>
			   
			   <span>Category:
				   <?php the_category(', ')?>
				</span>
			   
            <span>Tags:
				   <?php the_tags( '',', ','' ); ?>
				</span>
               <span>Author:<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a>
               <span>Date:<a href="<?php echo site_url() .'/'. get_the_date('Y/m/d'); ?>"><?php echo get_the_date(); ?></a>
                    <?php the_excerpt();?>
               <a class="readmore" href="<?php the_permalink();?>">Read More Content....</a>
			</div>

		 <?php endwhile; else : ?>
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		 <?php endif; ?>
         </div>

         <?php get_sidebar(); ?>

<?php get_footer();?>