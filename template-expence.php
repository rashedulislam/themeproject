<?php 
/* Template Name: Total Expences */
get_header(); ?>

<table class="table table-striped">
  <tr>
    <th>Expence Name</th>
    <th>Expence Category</th>
    <th>Amount</th>
  </tr>
  <?php 
  
  $total_expences = 0;
  $args = array(
    'post_type' => 'expence'
  );

  $the_query = new WP_Query( $args );
  //print_r($the_query);
   
    if ( $the_query->have_posts() ) :
       while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
          
        <tr>
            <td><?php the_title();?></td>
            <td><?php 
            $term_obj_list = get_the_terms( $post->ID, 'expence_category' );
            $terms_string = join(', ', wp_list_pluck($term_obj_list, 'name'));
              echo $terms_string;

            ?></td>
            <td><?php the_excerpt();?></td>
        </tr>
        <?php

        $cost = get_the_excerpt();
        $cost = (int)$cost;
        //print_r($cost);
        $total_expences += $cost;

        endwhile;
        wp_reset_postdata();
        else : ?>
            <p><?php _e( 'Sorry, no Expences right now.' ); ?></p>
        <?php endif; ?>

        <tr>
            <td>Total Amount</td>
            <td></td>
            <td><?php echo $total_expences; ?></td>
        </tr>

</table>


<?php get_footer();?>