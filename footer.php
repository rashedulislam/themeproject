   <div class="container">
       
       <div class="row">
         <div class="col-md-12">
            <div class="category-link">
               <span>Category:</span>
               <?php $categories = get_categories();
               //print_r($categories);
               foreach ($categories as $category): ?>
                  <a href="<?php echo get_category_link( $category->term_id ) ?>"> <?php echo $category->cat_name?> </a>
              <?php endforeach; ?>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="tag">
               <span>Tag:</span>

               <?php $tags = get_tags();
               //print_r($tags);
               foreach ($tags as $tag): ?>
                  <a href="<?php echo get_tag_link( $tag->term_id ) ?>"> <?php echo $tag->name . " "; ?></a>
              <?php endforeach;
               ?>
               
               
            </div>
         </div>
      </div>
   </div>
	<?php wp_footer();?>
   </div>
  </body>
</html>