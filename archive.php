<?php get_header(); ?>
 
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="catagory text-center">
               <?php if ( have_posts() ) :  ?>
                  <!-- <a href="">Category/</a> -->
                  <h2> <?php 
                    if(is_category()){
                        single_cat_title();
                    } elseif (is_tag()) {
                        single_tag_title();
                    } elseif (is_author()) {
                       the_post();
                       echo 'Author Archive: ' . get_the_author();
                       rewind_posts();
                    } elseif (is_day()) {
                       echo 'Daily Archive: ' . get_the_date();
                    } elseif (is_month()) {
                       echo 'Monthly Archive: ' . get_the_date('F Y');
                    } elseif (is_year()) {
                       echo 'Yearly Archive: ' . get_the_date('Y');
                    } else{
                       echo 'Archives: ';
                    }
                  ?> </h2>
            
            </div>
         </div>
      </div>

      <div class="row">
        
               <?php  while ( have_posts() ) : the_post(); 
               
               // $archive_year  = get_the_time('Y'); 
               // $archive_month = get_the_time('m'); 
               // $archive_day   = get_the_time('d'); 

               ?>
      <div class="col-md-6">
               <div class="title">
                     <div class="col-md-12">
                        <h2><?php the_title();?></h2>
                     </div>

                     <div class="col-md-6">
                     <span>Author:<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a></span>
                     </div>

                     <div class="col-md-6">
                     <span>Date:<a href="<?php echo site_url() .'/'. get_the_date('Y/m/d'); ?>"><?php echo get_the_date(); ?></a></span>
                     </div>

                     <div class="col-md-12">
                     <?php the_excerpt();?>
                     </div>
                     <div class="col-md-12 read-more">
                        <a href="<?php the_permalink();?>">Read More Content....</a>
                     </div>
               </div>

               <div class="col-md-12">
                  <h3> Posts except this post category and tag: </h3>
                  <ul>
                     <?php 

                        $cat_id = [];
                        $categories =get_the_category();
                        foreach ($categories as $category):
                           $cat_id[] = $category->cat_ID;
                        endforeach;

                        $tag_id = [];
                        $tags =get_the_tags();
                        foreach ($tags as $tag):
                           $tag_id[] = $tag->term_id;
                        endforeach;

                     $args = array(
                        'post_type'=>'post',
                        'category__not_in' => $cat_id,
                        'tag__not_in' => $tag_id,
                        'posts_per_page'=> 3,
                        'ignore_sticky_posts' => true
                     );
                     $the_query = new WP_Query( $args ); ?>
                      
                     <?php if ( $the_query->have_posts() ) : ?>
                         <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                          <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                         <?php endwhile; 
                          wp_reset_postdata(); ?>
                      
                     <?php else : ?>
                         <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                     <?php endif; ?>

                  </ul>
               </div>
        </div>

            <?php endwhile; else : ?>
                   <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
            
      </div>
  </div>

<?php get_footer();?>