<?php

if ( ! function_exists( 'practice_setup' ) ) :

    function practice_setup() {

        add_theme_support( 'title-tag' );

        add_theme_support( 'post-thumbnails' );

        register_nav_menus( array(
            'header_menu' => esc_html__( 'Primary', 'practice' ),
        ) );
    }
endif;
add_action( 'after_setup_theme', 'practice_setup' );


function practice_scripts() {
    wp_enqueue_style( 'bootstrap_min','https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css');
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
    wp_enqueue_style( 'main_style', get_stylesheet_uri() );

}
add_action( 'wp_enqueue_scripts', 'practice_scripts' );


function new_excerpt_more($more) {
    global $post;
 return '';
}
add_filter('excerpt_more', 'new_excerpt_more');


function posts_excerpt_length( $length ) {
    return 40;
}
add_filter( 'excerpt_length', 'posts_excerpt_length');



if ( ! file_exists( get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php' ) ) {
    return new WP_Error( 'class-wp-bootstrap-navwalker-missing', __( 'It appears the class-wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker' ) );
} else {
    require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';
}


add_action( 'init', 'codex_expences_init' );

function codex_expences_init() {
	$labels = array(
		'name'               => _x( 'Expences', 'post type general name', 'practice' ),
		'singular_name'      => _x( 'Expence', 'post type singular name', 'practice' ),
		'menu_name'          => _x( 'Expences', 'admin menu', 'practice' ),
		'name_admin_bar'     => _x( 'Expence', 'add new on admin bar', 'practice' ),
		'add_new'            => _x( 'Add New Expence', 'expence', 'practice' ),
		'add_new_item'       => __( 'Add New Expence', 'practice' ),
		'new_item'           => __( 'New Expence', 'practice' ),
		'edit_item'          => __( 'Edit Expence', 'practice' ),
		'view_item'          => __( 'View Expence', 'practice' ),
		'all_items'          => __( 'All Expences', 'practice' ),
		'search_items'       => __( 'Search Expences', 'practice' ),
		'parent_item_colon'  => __( 'Parent Expences:', 'practice' ),
		'not_found'          => __( 'No Expences found.', 'practice' ),
		'not_found_in_trash' => __( 'No Expences found in Trash.', 'practice' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Description.', 'practice' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'expence' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'excerpt' )
	);

	register_post_type( 'expence', $args );
}


add_action( 'init', 'expence_category_taxonomies', 0 );

function expence_category_taxonomies() {
	$labels = array(
		'name'              => _x( 'Expence Categories', 'taxonomy general name', 'practice' ),
		'singular_name'     => _x( 'Expence Category', 'taxonomy singular name', 'practice' ),
		'search_items'      => __( 'Search Expence Categories', 'practice' ),
		'all_items'         => __( 'All Expence Categories', 'practice' ),
		'parent_item'       => __( 'Parent Expence Category', 'practice' ),
		'parent_item_colon' => __( 'Parent Expence Category:', 'practice' ),
		'edit_item'         => __( 'Edit Expence Category', 'practice' ),
		'update_item'       => __( 'Update Expence Category', 'practice' ),
		'add_new_item'      => __( 'Add New Expence Category', 'practice' ),
		'new_item_name'     => __( 'New Genre Expence Category', 'practice' ),
		'menu_name'         => __( 'Expence Category', 'practice' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'expence_category' ),
	);

    register_taxonomy( 'expence_category', array( 'expence' ), $args );
}