<?php get_header(); ?>

<section class="main">
      <div class="container">

      <?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
<div class="row">
         <div class="col-md-8 title">
            <h2><?php the_title();?></h2>
            <span>category: <?php the_category(', ')?> </span>
            <span>Tags:
				   <?php the_tags( '',', ','' ); ?>
				</span>
               <span>Author:<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a></span>
               <span>Date:<a href="<?php echo site_url() .'/'. get_the_date('Y/m/d'); ?>"><?php echo get_the_date(); ?></a></span>
                  <?php the_content();?>
            
            <span>Next post:</span><?php next_post_link('%link', '%title');?>
            <span>Previous post:</span><?php previous_post_link('%link', '%title');?>
         </div>
         
         <?php endwhile; else : ?>
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
         <?php endif; ?>
         


 
         <div class="col-md-8">
            <div class="col-md-6">
               <h3>Related post</h3>
               <ul>

               <?php
               $cat_id = [];
               $categories =get_the_category();
               foreach ($categories as $category):
                  $cat_id[] = $category->cat_ID;
                  //$cat_id[] = $category->cat_name;
               endforeach;
               //print_r($cat_id);
               
               $args = array(
                  'post_type'=>'post',
                  'category__in' => $cat_id,
                  'posts_per_page'=> 5,
                  'ignore_sticky_posts' => true
               );

               $the_query = new WP_Query( $args ); ?>
                
               <?php if ( $the_query->have_posts() ) : ?>
 
                   <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                     <a href="<?php the_permalink(); ?>">
                        <li><?php the_title(); ?></li>
                     </a>
                   <?php endwhile; ?>
                   <?php wp_reset_postdata(); ?>
                
               <?php else : ?>
                   <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
               <?php endif; ?>
             </ul>
            </div>

            <div class="col-md-6">
               <h3>You may also like (not related)</h3>
               <ul>
                  <?php
                  $cat_id = [];
                  $categories =get_the_category();
                  foreach ($categories as $category):
                     $cat_id[] = $category->cat_ID;
                     //$cat_id[] = $category->cat_name;
                  endforeach;
                  //print_r($cat_id);
                  
                  $args = array(
                     'post_type'=>'post',
                     'category__not_in' => $cat_id,
                     'posts_per_page'=> 5,
                     'ignore_sticky_posts' => true
                  );

                  $the_query = new WP_Query( $args ); ?>
                  
                  <?php if ( $the_query->have_posts() ) : ?>
   
                     <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <a href="<?php the_permalink(); ?>">
                           <li><?php the_title(); ?></li>
                        </a>
                     <?php endwhile; ?>
                     <?php wp_reset_postdata(); ?>
                  
                  <?php else : ?>
                     <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                  <?php endif; ?>
              </ul>
            </div>
            <div class="col-md-6">
               <h3>Partially Related post</h3>
               <ul>
                  <?php
                  
                     $tag_id = [];
                     $tags =get_the_tags();
                     foreach ($tags as $tag):
                        $tag_id[] = $tag->term_id;
                     endforeach;
                     //print_r($tag_id);
                     
                     $args = array(
                        'post_type'=>'post',
                        'tag__in' => $tag_id,
                        'posts_per_page'=> 5,
                        'ignore_sticky_posts' => true
                     );

                     $the_query = new WP_Query( $args ); ?>
                     
                     <?php if ( $the_query->have_posts() ) : ?>
      
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                           <a href="<?php the_permalink(); ?>">
                              <li><?php the_title(); ?></li>
                           </a>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                     
                     <?php else : ?>
                        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                     <?php endif; ?>
               </ul>
            </div>
         </div>

         <?php get_sidebar();
         ?>

         </div>

     

<?php get_footer(); ?>