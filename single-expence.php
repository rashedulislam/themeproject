<?php get_header(); ?>

<section class="main">
<div class="container">

      <?php if ( have_posts() ) : while ( have_posts() ) : the_post();  ?>
<div class="row">
         <div class="col-md-8 title">

            <h2>Expence Name: <?php the_title();?></h2>

            <h3>Expence Category: <?php 
            $term_obj_list = get_the_terms( $post->ID, 'expence_category' );
            $terms_string = join(', ', wp_list_pluck($term_obj_list, 'name'));
              echo $terms_string;

            ?> </h3>

            <h3>Expence Date:<a href="<?php echo site_url() .'/'. get_the_date('Y/m/d'); ?>"><?php echo get_the_date(); ?></a></h3>

            <h3>Expence Details: <h3>

            <?php the_content();?>

            <h3>Expence Amount: <?php the_excerpt(); ?> </h3>

         </div>
         
         <?php endwhile; else : ?>
			  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
         <?php endif;
         
         get_sidebar();
         ?>
</div>

<?php get_footer(); ?>